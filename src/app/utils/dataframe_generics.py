import pandas as pd
import numpy as np
import csv
from django.http import HttpResponse, response
from django.conf import settings
from django.db import models
from datetime import datetime
from io import BytesIO
import pandas.io.formats.excel

class DataframeGenerics:
    """Generic methods to interact with dataframes using by preference\
        chained methods"""
    """2022-06-20"""
    def __init__(self) -> None:
        pass

    def convert_to_date(self, df: pd.DataFrame, target_columns: dict = {}) -> pd.DataFrame:
        """This method converts a column to date"""

        if not target_columns:
            target_columns = {coluna:'%Y-%m-%d' for coluna in df.columns if coluna.startswith('data_')}

        if not isinstance(target_columns, dict):
            print(f"[ *** warning *** convert_to_date: target_columns must be a dictionary!]")
            return df

        for coluna, frmt in target_columns.items():            
            if coluna not in df.columns:
                print(f"[ *** warning *** convert_to_date: {coluna} doesn't exist!]")
                continue
            if frmt == '':
                frmt=None
            
            # if pd.api.types.is_datetime64_any_dtype(df[coluna]):
            #     df[coluna] = (df[coluna].astype('datetime64[D]'))
            # else:
            #     df[coluna] = (
            #         pd.to_datetime(
            #             df[coluna].fillna(pd.NaT),
            #             format=frmt,
            #             errors="coerce")
            #         .astype('datetime64[D]')
            #     )

            if pd.api.types.is_datetime64_any_dtype(df[coluna]):
                df[coluna] = df[coluna].dt.date
            else:
                df[coluna] = pd.to_datetime(
                    df[coluna].fillna(pd.NaT), format=frmt, errors='coerce').dt.date

        return df

    def convert_to_float(self, df: pd.DataFrame, target_columns: list = [], decimals: int = None) -> pd.DataFrame:
        """This method converts the target_columns to float64 (n decimals)"""
        if not target_columns:
            target_columns = [coluna for coluna in df.columns if coluna.startswith('valor_')]
        
        if isinstance(target_columns, str):
            target_columns = target_columns.split(",")
        
        for coluna in target_columns:
            if coluna not in df.columns:
                print(f"[ *** warning *** convert_to_float: {coluna} doesn't exist!]")
                continue
            if not pd.api.types.is_float_dtype(coluna):
                df[coluna] = pd.to_numeric(
                    df[coluna], errors='coerce').astype('float', errors='ignore')
            if decimals:
                df[coluna] = df[coluna].round(decimals)
        return df

    def convert_to_first_month_day(self, df: pd.DataFrame, target_columns: list) -> pd.DataFrame:
        """This method converts a datetime column to the first day of month
        """

        if isinstance(target_columns, str):
            target_columns = target_columns.split(",")

        for coluna in target_columns:
            if coluna in df.columns:
                df[coluna] = pd.to_datetime(
                    df[coluna],
                    format='%Y-%m-%d',
                    errors='coerce'
                ).dt.to_period('M').dt.to_timestamp()

        return df
    
    def convert_to_int(self, df: pd.DataFrame, target_columns: list) -> pd.DataFrame:
        """This method converts a column to int64
        """
        if isinstance(target_columns, str):
            target_columns = target_columns.split(",")

        for coluna in target_columns:
            if coluna not in df.columns:
                print(f"[ *** warning *** convert_to_int: {coluna} doesn't exist!]")
                continue
            if not pd.api.types.is_integer_dtype(coluna):
                df[coluna] = pd.to_numeric(
                    df[coluna], errors='coerce').astype('int64', errors='ignore')

        return df

    def optimize_to_category(self, df: pd.DataFrame, target_columns: list) -> pd.DataFrame:
        """This method optimize a dataframe by converting a column into category
        """

        for coluna in target_columns:
            if coluna not in df.columns:
                print(f"[ *** warning *** optimize_to_category: {coluna} doesn't exist!]")
                continue
            if not pd.api.types.is_categorical_dtype(df[coluna]):
                df[coluna] = df[coluna].astype('category', errors='ignore')
        return df

    def convert_case(self, df, mode: str, target_columns: list) -> pd.DataFrame:
        """This method convert a string Series into upper or lower case
        """
        
        if df.empty:
            return df
        
        for coluna in target_columns:
            if coluna not in df.columns:
                print(f"[ *** warning *** convert_case: {coluna} doesn't exist!]")
                continue
            if pd.api.types.is_string_dtype(df[coluna]):
                if mode == 'upper':
                    df[coluna] = df[coluna].str.upper()
                elif mode == 'lower':
                    df[coluna] = df[coluna].str.lower()
            else:
                print(f"[ *** warning *** convert_case: {coluna} is not of string dtype!]")

        return df

    def convert_to_object(self, df, target_columns: list) -> pd.DataFrame:
        """This method convert a Series into pbject (string)
        """
        
        for coluna in target_columns:
            if coluna not in df.columns:
                print(f"[ *** warning *** convert_to_object: {coluna} doesn't exist!]")
                continue
            if not pd.api.types.is_string_dtype(df[coluna]):
                df[coluna] = df[coluna].astype('str')

        return df

    def prepare_to_json(self, df: pd.DataFrame, target_columns: list = []) -> pd.DataFrame:
        """This method apply transformations to the dataframe in order to 
        prepare it to be exported as a json file."""

        if not target_columns:
            target_columns = [col for col in df.columns if col.startswith('id_') or col.endswith('_id')]
        for coluna in target_columns:
            if not coluna in df.columns:
                print(f"[ *** warning *** prepare_to_json: {coluna} doesn't exist!]")
                continue
            df[coluna] = pd.to_numeric(df[coluna], errors="coerce").astype('Int64')
        for coluna in df.select_dtypes("category").columns:
            if df[coluna].isna().any():
                df[coluna] = df[coluna].astype("object")
        df.replace({np.nan:None, pd.NA: None}, inplace=True)

        return df

    def round_float_to_export(
        self, df: pd.DataFrame, target_columns: list =[], decimals: int =2) -> pd.DataFrame:
        """This method converts a column to double decimals floats, in order to export
        the dataframe (i.e.: to json)"""
        if df.empty:
            return df
        if not target_columns:
            target_columns = [col for col in df.columns if col.startswith('valor')]

        for coluna in target_columns:
            if coluna not in df.columns:
                print(f"[ *** warning *** round_float_to_export: {coluna} doesn't exist!]")
                continue
            df[coluna] = pd.to_numeric(
                df[coluna], errors='coerce').astype('float64').round(decimals)

        return df

    def compare_df_to_insert(
        self,
        df: pd.DataFrame,
        df_reference: pd.DataFrame,
        join_columns: dict,
        target_column: str,
        final_columns: list) -> pd.DataFrame:

        for column, datatype in join_columns.items():
            for dtf in [df, df_reference]:
                if column in dtf.columns:
                    dtf[column] = dtf[column].astype(datatype, errors='ignore')
        df = pd.merge(
            df, df_reference,
            how='left', on=list(join_columns.keys()))

        # If target column is null, the data doesn't exist in the reference table
        # Returns only new data
        has_not_db = df[target_column].isna()

        return df.loc[has_not_db, final_columns].reset_index(drop=True)

    def export_csv(
        self,
        df: pd.DataFrame,
        filename: str = "file",
        extension: str = ".csv") -> response:
        
        response = HttpResponse(content_type='text/csv')
        response["Content-Disposition"] =\
            f"attachment; filename={filename}_{datetime.now().strftime('%Y%m%d_%H%M%S')}{extension}"
        df.to_csv(
            path_or_buf=response,
            sep=';',
            encoding='utf-8',
            quoting=csv.QUOTE_NONNUMERIC,
            index=False)

        return response

    def load_db(
        self, df: pd.DataFrame, target_model: models.Model, target_columns: list = None) -> dict:
        """This method loads the dataframe data in a model
            This method doesn't work with models that have FK fields
        """

        log = {
                'success': False,
                'details': None,
                'started_at': datetime.now(),
                'finished_at': None,
                'inserted': 0
            }

        if not target_columns:
            target_columns = df.columns

        for coluna in target_columns:
            if coluna not in df.columns:
                log["details"] = f"A coluna {coluna} não existe no dataframe inputado!"
                return log

        if not df.empty:
            # Parâmetros:
            values_list = (
                df.replace({np.nan: None})
                    .loc[:, target_columns]
                    .reset_index(drop=True)
                    .to_dict('records')
            )
            
            # Create the django list from dict
            objs = [target_model(**vals) for vals in values_list]
            try:
                target_model.objects.bulk_create(objs=objs, batch_size=500)
                log['inserted'] = len(values_list)
                log['details'] = 'Sucesso!'
                log['success'] = True
            except Exception as err:
                print(str(err))
                log['details'] = f'Falha na inserção dos dados: {str(err)}'
        else:
            log['details'] = "Dataframe vazio!"
        
        log['finished_at'] = datetime.now()

        return log

    def export_xlsx(
        self,
        df: pd.DataFrame,
        file_name: str = "file",
        sheet_name: str = "base",
        columns_formats: list = None):

        FONTE = "Calibri"
        pd.io.formats.excel.ExcelFormatter.header_style = None
        FORMATS = {
                "date": {
                    "num_format": "dd/mm/yyyy"
                },
                "date_time": {
                    "num_format": "dd/mm/yyyy hh:mm:ss"
                },
                "money": {
                    "num_format": '''_-R$ * #,##0.00_-;-R$ * #,##0.00_-;_-R$ * "-"??_-;_-@_-'''
                },
                "code": {
                    # "bold": True,
                    "num_format": "##################"
                },
                "decimal": {
                    "num_format": "#,##0.00"
                },
                "percent": {
                    "num_format": "##0.00%"
                }
            }

        # If the columns_formats doesnt exists, it's created from the df columns:
        if not columns_formats:
            columns_formats = [dict({"column": c}) for c in df.columns]
        
        # If the columns_formats exists, but doesnt contains some df column, the basic format is added for it
        for column in df.columns:
            element = [c for c in columns_formats if c["column"] == column]
            if not element:
                columns_formats.append({"column": column})
        # Ensure the only the columns in the df are in the columns formats:
        columns_formats = [i for i in columns_formats if i["column"] in df.columns]

        # Add some basic formats to the columns if it doesnt exists:
        for col in columns_formats:
            if col.get("format", None):
                continue
            if col["column"].startswith(("data", "dt")):
                col.update({"format": "date"})
            elif col["column"].endswith(("_em", "_at")):
                col.update({"format": "date_time"})
            elif col["column"].startswith("id_") or col["column"].endswith("_id"):
                col.update({"format": "code"})
            elif col["column"].startswith(("valor","vr_", "percent")):
                col.update({"format": "decimal"})

        vp6_color = '#104288'
        main_color = getattr(settings, 'CLIENT_COLORS', {}).get('bg_color', vp6_color)

        with BytesIO() as content:
            writer = pd.ExcelWriter(
                content,
                engine='xlsxwriter',
                # date_format='dd/mm/yyyy',
                # datetime_format='dd/mm/yyyy hh:mm:ss'
                )

            df.to_excel(writer, index=False, startrow=0, startcol=0, sheet_name=sheet_name)

            workbook = writer.book
            worksheet = writer.sheets[sheet_name]
            
            header_format = workbook.add_format(
                {
                    'bold': True,
                    'font_color': main_color,
                    'bottom': 5,
                    'bottom_color': main_color,
                    'font_name': FONTE,
                    'font_size': 10})
            
            arr = df.to_numpy()
            # Define o formato das células de acordo com as colunas:
            for col_index, col in enumerate(df.columns):
                
                frmt = {}
                for item in columns_formats:
                    if item["column"] == col:
                        frmt = item

                max_values_length = int(df[col].astype('str').str.len().max()) if not df.empty else 10
                max_width = max(max_values_length, len(col)) * 1.2

                # Define o formato da coluna a partir dos FORMATOS existentes
                col_format = workbook.add_format(
                    dict(
                        FORMATS.get(frmt.get("format", ""), {}),
                            **{
                                'font_name': FONTE,
                                'font_size': 9
                            }
                        )
                    )

                if frmt.get("format", "").startswith('date'):
                    for row in range(0, len(arr)):
                        worksheet.write(row + 1, col_index, arr[row, col_index], col_format)
                worksheet.set_column(col_index, col_index, int(max_width), col_format)
                
                # Define o formato da célula do cabeçalho
                value = frmt.get('column') if not frmt.get('rename', None) else frmt.get('rename')
                worksheet.write(0, col_index, value, header_format)
            
            # Define o autofilter # 0,0=A1 e 0,len = última célula da primeira linha
            worksheet.autofilter(0, 0, 0, len(df.columns)-1) 
            # Congela a primeira linha
            worksheet.freeze_panes(1, 0)
            writer.save()
            
            # Add timestamp and extension to the filename
            file_name = f"{file_name}_{datetime.now().strftime('%Y%m%d_%H%M%S')}.xlsx"

            response = HttpResponse(content.getvalue(), content_type='application/vnd.ms-excel')
            response['Content-Disposition'] = 'attachment; filename={}'.format(file_name)

            return response
    
    def prepare_to_export(self, df: pd.DataFrame, **kwargs) -> pd.DataFrame:
        """This method apply type format transformations to the dataframe values, \
            in order to convert then to python standard types so that the dataframe \
                can be exported to a file."""
        target_columns = kwargs.get("target_columns", [])
        if target_columns:
            for column in target_columns:
                if column.get("name", "") in df.columns:
                    if column.get("type") == "decimal":
                        df = self.convert_to_float(
                            df, target_columns=[column["name"]], decimals=column.get("decimals", 2)
                            )
                    if column.get("type") == "integer":
                        df[column["name"]] = pd.to_numeric(df[column["name"]], errors="coerce").astype('Int64')
                else:
                    print(f"[ *** warning *** prepare_to_export: {column.get('name', '')} doesn't exist!]")

        for coluna in df.select_dtypes("category").columns:
            if df[coluna].isna().any():
                df[coluna] = df[coluna].astype("object")
        return df.replace({np.nan: None, pd.NA: None})
    
    def model_dtype_map(model: models.Model, fields: (list or tuple) = None) -> dict:
        """Has a mapping between Model field types and pandas dataframes dtypes

        Args:
            model (Model): A Django Model instance
            fields (list or tuple, optional): list of field names [str] that'll be mapped. Defaults to None.

        Returns:
            dict: Dictionary containing field names and pandas datatypes for dataframes
        """
        data_types = {
            'AutoField': 'Int32',
            'BigAutoField': 'Int64',
            'BigIntegerField': 'Int64',
            'BinaryField': 'object',
            'BooleanField': 'bool',
            'CharField': 'object',
            'DateField': 'datetime64[ns]',
            'DateTimeField': 'datetime64[ns]',
            'DecimalField': 'float',
            'DurationField': 'timedelta[64]',
            'FileField': 'object',
            'FilePathField': 'object',
            'FloatField': 'float',
            'IntegerField': 'Int32',
            'IPAddressField': 'object',
            'GenericIPAddressField': 'object',
            'NullBooleanField': 'bool',
            'OneToOneField': 'Int64',
            'PositiveIntegerField': 'Int32',
            'PositiveSmallIntegerField': 'Int16',
            'SlugField': 'object',
            'SmallAutoField': 'Int16',
            'SmallIntegerField': 'Int16',
            'TextField': 'object',
            'TimeField': 'timedelta64[ns]',
            'UUIDField': 'object',
            'JSONField': 'object',
        }
        datatype_map = {}
        if not fields:
            fields = (f.name for f in model._meta.fields)
        
        for fieldname in fields:
            if len(fieldname.split('__')) == 1:
                field = model._meta.get_field(fieldname)
                if getattr(field, 'related_model'):
                    if hasattr(field, 'to_field'):
                        field = field.related_model._meta.get_field(field.to_field)
                    else:
                        field = field.related_model._meta.pk
            else:
                _model = model
                for _fieldname in fieldname.split('__'):
                    field = _model._meta.get_field(_fieldname)
                    if getattr(field, 'related_model'):
                        _model = field.related_model
            
            datatype_map.update(
                **{
                    fieldname: data_types.get(field.get_internal_type(), 'object')
                }
            )
        return datatype_map
