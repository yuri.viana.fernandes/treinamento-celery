import numpy as np
import pandas as pd
import json
from app.utils.dataframe_generics import DataframeGenerics
from datetime import datetime
from django.db import models, connections
from django.utils import timezone
from typing import Tuple, List
from traceback import format_tb


class PipelineLog(object):
    '''Representação de um log com os atributos de uma pipeline de dados'''
    def __init__(self, name: str, params: dict = None, task_id: str = None, **kwargs) -> None:
        self.name = name
        self.started_at = timezone.now()
        self.finished_at: datetime = None
        self.success: bool = False
        self.n_extracted: int = 0
        self.n_inserted: int = 0
        self.n_deleted: int = 0
        self.n_updated: int = 0
        self.detail: str = None
        self.status: str = None
        self.params = params
        self.task_id = task_id
        for k,v in kwargs.items():
            setattr(self, k, v)

    def __str__(self) -> str:
        return self.__repr__

    def __repr__(self):
        return repr(self.__dict__)
    
    def keys(self) -> list:
        return self.__dict__.keys()

    def values(self) -> list:
        return self.__dict__.values()
    
    @property
    def data(self) -> dict:
        repr = self.__dict__
        for k in list(repr.keys()):
            if isinstance(repr[k], models.Model):
                repr[k] = str(repr[k].pk)
            elif k == 'params':
                try:
                    repr[k] = json.loads(repr[k])
                except:
                    pass
        return repr
    
    def set_dblog(self, model_instance: models.Model) -> None:
        '''Define um Model onde a pipeline irá armazenar o log'''
        self.model_class: str = model_instance.__name__
        self.model_instance = model_instance()
        self._save_dblog()
    
    def clear(self) -> None:
        for k in self.keys():
            if isinstance(getattr(self, k), bool):
                value = False
            elif isinstance(getattr(self, k), int):
                value = 0
            else:
                value = None
            setattr(self, k, value)

    def terminate(self, **kwargs) -> None:
        self.finished_at = timezone.now()
        for k,v in kwargs.items():
            if hasattr(self, k):
                setattr(self, k, v)

    def save(self) -> None:
        if getattr(self, 'model_instance', None):
            self._save_dblog()

    def _save_dblog(self) -> None:
        for k,v in self.__dict__.items():
            if k in [field.name for field in type(self.model_instance)._meta.fields]:
                setattr(self.model_instance, k, v)
        self.model_instance.save()


class DataPipeline(DataframeGenerics):
    status: str
    dataset: pd.DataFrame
    data_to_insert: pd.DataFrame
    data_to_update: pd.DataFrame
    params: dict
    log: PipelineLog
    
    def __init__(self, **kwargs):
        self.params = kwargs.get('params', {})
        self.task_id = kwargs.get('task_id')
        self.status = None
        self.log = PipelineLog(
            name=self._process_name,
            task_id=self.task_id,
            params=json.dumps(self.params, default=str)
        )

    def __enter__(self):
        return self
    
    def __exit__(self, type, value, traceback):
        self.log.finished_at = timezone.now()
        if type:
            msg = "{} FAILED: {}\n{}\n".format(
                    self.status,
                    str(value),# + " - " + value.__repr__(),
                    "".join(format_tb(traceback)),
                )
            self.log.detail = msg if self.log.detail is None\
                else (self.log.detail + f"\n{msg}")
        else:
            self.log.success = True
        self.log.save()

    def load(self) -> None:
        """Generic method holder for loading data into the database"""
        pass

    @staticmethod
    def _parse_existing_and_non_null_fields(model: models.Model, values: dict) -> dict:
        return {k:v for k, v in values.items() if k in [m.name for m in model._meta._get_fields()] and v is not None}
    
    @staticmethod
    def _split_update_batchs(records: list) -> dict:
        """Retorna um dicionário, separando em chaves sequencias, lotes de registros que tem campos
            que serão atualizados de formas distintas.
            Isso permite que nenhum valor nulo seja setado no update"""
        update_batches = {}
        idx = 0
        for record in records:
            key_found = False
            rec_fields = [k for k in record.keys()]
            
            if update_batches == {}:
                update_batches[idx] = {
                    "fields": rec_fields,
                    "records": [record]
                }
                idx += 1
                continue
            
            for i in list(update_batches.keys()):
                if update_batches[i]["fields"] == rec_fields:
                    update_batches[i]["records"] += [record]
                    key_found = True
            
            if key_found == False:
                update_batches[idx] = {
                    "fields": rec_fields,
                    "records": [record]
                }
                idx += 1
        return update_batches
    
    @staticmethod
    def _nonnull_and_changed_values(vals: pd.Series, reference_vals: pd.Series) -> pd.Series:
        """Recebe um conjunto de valores [vals] e compara a um conjunto de referência [reference_vals] e no caso onde \
            [vals] não é nulo e diferente de [reference_vals] retorna o valor de [vals]. \
            Caso contrário, retorna os valores de [reference_vals]"""
        return np.where(
                (vals.notna()) & (vals != reference_vals),
                vals,
                reference_vals
            )
    
    @property
    def _process_name(self) -> str:
        app = self.__module__.split('.')[0]
        task = self.__class__.__name__
        return f'{app}.{task}'
    
    def _fetch_data_from_query(self, query: str, connection: str) -> Tuple[List, List]:
        result = []
        columns = []
        with connections[connection].cursor() as cursor:
            cursor.execute(query)
            result = cursor.fetchall()
            columns = [i[0] for i in cursor.description]
        return (result, columns)
    
    @staticmethod
    def _parse_to_datetime(date_arg) -> None:
        if isinstance(date_arg, str):
            return datetime.strptime(date_arg, '%Y-%m-%d')
        return date_arg
    
    @staticmethod
    def _extract_digits(alphanumeric: pd.Series) -> pd.Series:
        return alphanumeric.str.replace(r'[^0-9]+', '', regex=True).replace('', None)

    @staticmethod
    def _parse_to_date(series: pd.Series) -> pd.Series:
        return (
            pd.to_datetime(
                series,
                format='%Y%m%d',
                errors='coerce')
            .astype('datetime64[D]')
        )
    
    def _select_only_changed_values(self, df: pd.DataFrame) -> pd.DataFrame:
        """Retorna somente os dados novos que existem (não-nulos) e são diferentes na comparação"""
        if df.empty:
            return pd.DataFrame()
        
        # Para cada uma das colunas comparáveis, é feito um teste se existe
        # dado novo e se ele é diferente do existente
        for coluna in [c for c in df.columns if c.endswith('_existing')]:
            new_values = df[coluna.replace("_existing", "")]
            db_values = df[coluna]

            df[coluna.replace("_existing", "_test")] = np.where(
                (new_values.notna()) & (new_values != db_values), True, False
            )
        # Checa se algum dos testes foi positivo
        has_new_data = df[[c for c in df.columns if c.endswith("_test")]].any(axis=1)
        return (
            df[has_new_data]
            .reindex(
                columns=
                [
                    c for c in df.columns
                    if not c.endswith(('_existing', '_test'))
                ]
            )
            .reset_index(drop=True)
        )
    
    @staticmethod
    def _replace_with_fk(series: pd.Series, model: models.Model, field: str) -> pd.Series:
        _fk_map = {
            item[field]: item['id']
            for item in (
                model
                .objects
                .filter(**{f'{field}__in': series.dropna().unique().tolist()})
                .values(field, 'id')
            )
        }
        return series.map(_fk_map)