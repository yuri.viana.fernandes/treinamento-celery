from django.db import models


class Receita(models.Model):
    data = models.DateField()
    valor = models.DecimalField(max_digits=11, decimal_places=2, default=0.0)
    quantidade = models.IntegerField(default=0)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'receita_venda'
        verbose_name = 'venda receita'
        verbose_name_plural = 'vendas receita'

    def __str__(self) -> str:
        return f"{self.data} - {self.valor} - {self.quantidade}"
