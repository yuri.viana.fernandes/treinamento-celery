from django.db import models


class ReceitaConsolidacao(models.Model):
    data = models.DateField()
    valor = models.DecimalField(max_digits=14, decimal_places=2)
    centro_custo = models.CharField(max_length=200, unique=True)
    dre_subgrupo = models.CharField(max_length=200, unique=True)
    versao = models.CharField(max_length=200, unique=True)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = 'receita_consolidacao'
        verbose_name = 'Consolidação da Receita'
        verbose_name_plural = 'Consolidação da Receita'
