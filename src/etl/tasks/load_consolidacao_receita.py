import numpy as np
import pandas as pd
from celery import shared_task
from django.db import transaction

from app.utils.pipeline import DataPipeline

from ..models import ReceitaConsolidacao


class LoadConsolidacaoReceita(DataPipeline):
    """Processo que faz a carga no model [ReceitaConsolidacao]"""

    def __init__(self, **kwargs) -> None:
        """Inicialização da classe"""
        self.params = {}

        super().__init__(params=self.params, **kwargs)

    def run(self) -> dict:
        """Executa o processo"""
        self.extract_transform_dataset()
        self.bulk_load_data()

        self.log.terminate()
        return self.log

    def extract_transform_dataset(self) -> None:
        """Extrai e transforma o dataset principal"""
        self.dataset = pd.DataFrame()

    @transaction.atomic
    def bulk_load_data(self):
        """Realiza a carga em lote dos dados na model [PesConsolidacaoColaborador]"""
        self.delete_data()
        if not self.dataset.empty:
            objs = [
                ReceitaConsolidacao(**vals)
                for vals in
                self.dataset.replace(
                    {np.nan: None, pd.NA: None}).to_dict('records')
            ]

            ReceitaConsolidacao.objects.bulk_create(objs=objs, batch_size=500)
            self.log.n_inserted = len(objs)

    def delete_data(self):
        """Realiza o delete dos dados a serem substituidos no model [ReceitaConsolidacao]"""
        filtros = {}
        n_deleted, __ = (
            ReceitaConsolidacao
            .objects
            .filter(**filtros)
            .delete()
        )
        self.log.n_deleted = n_deleted


# DEFINIÇÃO DA TASK DO CELERY
# NAME: NOME DA TASK A SER EXECUTADA NO CELERY, DEVE SER ÚNICO.
# BIND:
# AUTORETRY_FOR:
# RETRY_BACKOFF:
# RETRY_KWARGS: QUANTIDADE DE VEZES QUE O CELERY TENTARÁ EXECUTAR A TASK ATÉ OBTER SUCESSO

@shared_task(
    name='receita.load_consolidacao_receita',
    bind=True,
    autoretry_for=(Exception,),
    retry_backoff=5,
    retry_kwargs={'max_retries': 3})
def load_consolidacao_receita_async(self, **kwargs) -> dict:
    """Executa a task de forma assíncrona"""
    with LoadConsolidacaoReceita(task_id=self.request.id, **kwargs) as load:
        log = load.run()
    return log.data
