from celery import chain
from django.db.models.signals import post_delete, post_save
from django.dispatch import receiver

from ..models import Receita
from ..tasks import load_consolidacao_receita_async


def workflow(**params):
    return chain(
        load_consolidacao_receita_async.si(**params))


@receiver([post_save, post_delete], sender=Receita)
def changes_on_receita(sender, instance, created=False, *args, **kwargs):
    """Alterações no modelo receita acionam o recálculo da consolidação de receita"""
    params = {}
    workflow(**params)()
