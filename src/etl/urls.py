from django.urls import path
from rest_framework.routers import DefaultRouter

from .api import views

router = DefaultRouter()

urlpatterns = [
    path('consolidacao/load/', view=views.LoadConsolidacaoReceitaView.as_view()),
]
urlpatterns += router.urls
