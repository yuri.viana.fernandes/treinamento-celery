from django.shortcuts import get_object_or_404
from rest_framework.response import Response
from rest_framework.status import HTTP_202_ACCEPTED
from rest_framework.views import APIView

from ...tasks import LoadConsolidacaoReceita, load_consolidacao_receita_async


class LoadConsolidacaoReceitaView(APIView):

    def post(self, request, *args, **kwargs) -> Response:
        executar_tipo = request.data.get('executar_tipo')
        if executar_tipo == "Sincrono":
            with LoadConsolidacaoReceita() as task:
                log = task.run()
            return Response(log.data)
        else:
            task = load_consolidacao_receita_async.delay()
            return Response(
                {
                    "message": "A requisição foi recebida e a carga foi iniciada!",
                    'task': task.id,
                },
                status=HTTP_202_ACCEPTED
            )
